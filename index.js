console.log("Hello World");


	// 1. Create variables to store to the following user details:

	// -first name - String
	// -last name - String
	// -age - Number
	// -hobbies - Array
	// -work address - Object
	let firstName = 'John';
	console. log (firstName);
	let lastName = 'Smith';
	console. log (lastName);
	let age = 30;
	console. log (age);
	let hobbies = ['Biking', 'Mountain Climbing', 'Swimming']
	console. log (hobbies);
// 	let person = {
 // 	Workaddress:{
 // 		houseNumber: '32',
 // 		street: 'washington',
 // 		city: 'Lincoln',
 // 		state: 'Nebraska'
 // 	}
 // }
 // console. log (person);
 
	// 	-The hobbies array should contain at least 3 hobbies as Strings.
	// 	-The work address object should contain the following key-value pairs:

	// 		houseNumber: <value>
	// 		street: <value>
	// 		city: <value>
	// 		state: <value>

	// Log the values of each variable to follow/mimic the output.

	// Note:
	// 	-Name your own variables but follow the conventions and best practice in naming variables.
	// 	-You may add your own values but keep the variable names and values Safe For Work.


	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "My full name is:" + "Steve Rogers";
	console.log(fullName);

	let number = "My current age is:" + 40;
	console.log(number);
	
	let friends = ['My Friends are:'+'Tony','Bruce','Thor','Natasha','Clint', 'Nick'];
	console.log(friends);

// 	let message = {
// 		name: 'My Full Profile:',
// 		userName 'captain_america',
// 		fullName: 'Steve Rogers',
// 		age: 40,
// 		isActive: false
// }
	
let message = {
 	myFullProfile:{
 		userName: 'captain_america',
 		age: '40',
 		isActive: false,

 	}
 }
 console. log (message);
	


// 	let person = {
 // 	fullName: 'Juan Dela Cruz',
 // 	age: 35,
 // 	isMarried: false,
 // 	contact: ["+123 456 12379", "874 1258"],
 // 	address: {
 // 		houseNumber: '345',
 // 		city: 'Manila'
 // 	}
 // }
 // console. log (person);

	let name = "My bestfriend is: "+"Bucky Barnes";
	console.log(name);

	const lastLocation = "I was found frozen in: "+ "Arctic Ocean";
	console.log(lastLocation);